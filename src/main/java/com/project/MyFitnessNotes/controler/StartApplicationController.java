package com.project.myFitnessNotes.controler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class StartApplicationController {

    @GetMapping("index")
    public ModelAndView startApplication(){
        return new ModelAndView("index");
    }
}
