package com.project.myFitnessNotes.service.mapper;

import com.project.myFitnessNotes.model.Note;
import com.project.myFitnessNotes.model.Product;
import com.project.myFitnessNotes.model.User;
import com.project.myFitnessNotes.repository.UserRepository;
import com.project.myFitnessNotes.service.dto.CreateUpdateNoteDto;
import com.project.myFitnessNotes.service.dto.NoteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class NoteToMapper {
    private static Integer idCounter = 0;
    @Autowired
    private UserRepository repositoryUser;


    public NoteDto toDto(Note note){


        List<Integer> productsList = new ArrayList<>();
        if(note.getProducts() !=null){
            for(Product product: note.getProducts()){
                productsList.add(product.getCode());
            }
        }
        User userFromDb = repositoryUser.findById(note.getId()).get();
        return NoteDto.builder()
                .id(note.getId())
                .text(note.getText())
                .createdAt(OffsetDateTime.now())
                .updateAt(null)
                .productIds(productsList)
                .userId(userFromDb.getId())
                .build();

    }

    public Note toModel (CreateUpdateNoteDto updateNote){

        User user = repositoryUser.findById(updateNote.getUserId()).get();
        return Note.builder()
                .id(idCounter++)
               .text(updateNote.getText())
                .createdAt(OffsetDateTime.now())
                .updateAt(null)
                .products(null)
                .user(user)
                .build();


    }
}
