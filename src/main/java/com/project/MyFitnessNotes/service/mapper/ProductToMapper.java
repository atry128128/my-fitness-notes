package com.project.myFitnessNotes.service.mapper;

import com.project.myFitnessNotes.model.Note;
import com.project.myFitnessNotes.model.Product;
import com.project.myFitnessNotes.model.User;
import com.project.myFitnessNotes.repository.ProductRepository;
import com.project.myFitnessNotes.service.dto.CreateUpdateProductDto;
import com.project.myFitnessNotes.service.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductToMapper {
    private static Integer code = 0;
    @Autowired
    private ProductRepository repository;

    public ProductDto toDto(Product product) {

        List<Integer> usersList = new ArrayList<>();
        if(product.getUsersList() !=null){
            for(User user: product.getUsersList()){
                usersList.add(user.getId());
            }
        }
        List<Integer> notesList = new ArrayList<>();
        if(product.getNotes() !=null){
            for(Note note: product.getNotes()){
                usersList.add(note.getId());
            }
        }
        return ProductDto.builder()
                .code(product.getCode())
                .name(product.getName())
                .type(product.getType())
                .amountOfCalories(product.getAmountOfCalories())
                .amountOfProtein(product.getAmountOfProtein())
                .amountOfFat(product.getAmountOfFat())
                .productProducer(product.getProductProducer())
                .userIds(usersList)
                .noteIds(notesList)
                .build();

    }

    public Product toModel(CreateUpdateProductDto createProduct) {



        return Product.builder()
                .code(code++)
                .name(createProduct.getName())
                .type(createProduct.getType())
                .amountOfCalories(createProduct.getAmountOfCalories())
                .amountOfProtein(createProduct.getAmountOfProtein())
                .amountOfFat(createProduct.getAmountOfFat())
                .productProducer(createProduct.getProductProducer())
                .usersList(null)
                .notes(null)
                .build();


    }
}
