package com.project.myFitnessNotes.service;

import com.project.myFitnessNotes.model.User;
import com.project.myFitnessNotes.repository.UserRepository;
import com.project.myFitnessNotes.service.dto.CreateUserDto;
import com.project.myFitnessNotes.service.dto.UpdateUserDto;
import com.project.myFitnessNotes.service.dto.UserDto;
import com.project.myFitnessNotes.service.exception.BusyLoginException;
import com.project.myFitnessNotes.service.exception.UserDoesNotExistException;
import com.project.myFitnessNotes.service.mapper.UserToMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserToMapper mapper;
    @Autowired
    private UserRepository repository;

    private Integer id = 1;


    public List<UserDto> getAllUsers() {
        List<User> usersFromDb = repository.findAll();
        List<UserDto> usersDto = new ArrayList<>();
        for (User user : usersFromDb) {
            UserDto userDto = mapper.toDto(user);

            usersDto.add(userDto);
        }
        return usersDto;
    }

    public UserDto findUserById(int id) throws UserDoesNotExistException {
        Optional<User> userFromDb = repository.findById(id);
        if (userFromDb.isPresent()) {
            return mapper.toDto(userFromDb.get());
        }
        throw new UserDoesNotExistException("User with id: " + id + " does not exist");
    }

    public UserDto addNewUser(CreateUserDto createUserDto) throws BusyLoginException {
        List<User> usersFromDb = repository.findAll();

        for (User user : usersFromDb) {
            if (user.getLogin().equalsIgnoreCase(createUserDto.getLogin())) {
                throw new BusyLoginException("Login: " + createUserDto.getLogin() + " is busy. Choose another.");
            }
        }
        repository.save(mapper.toModel(createUserDto));

        User user = new User(id++, createUserDto.getLogin(), createUserDto.getPassword(), createUserDto.getName(), createUserDto.getSurname(), createUserDto.getRole(),
                createUserDto.getSex(), createUserDto.getWeight(), createUserDto.getHeight(),
                createUserDto.getDateOfBirth(), createUserDto.getDateOfFirstEntry(),null,null);
        return mapper.toDto(user);

    }

    public UserDto changeUser(int id, UpdateUserDto updateUser) throws UserDoesNotExistException {
        List<User> usersFromDb = repository.findAll();
        for (User user : usersFromDb) {
            if (id == user.getId()) {
                user.setPassword(updateUser.getPassword());
                user.setName(updateUser.getName());
                user.setSurname(updateUser.getSurname());
                user.setWeight(updateUser.getWeight());
                user.setHeight(updateUser.getHeight());
            }
            User savedUser = repository.save(user);
            return mapper.toDto(savedUser);
        }
        throw new UserDoesNotExistException("User with id: " + id + " does not exist");
    }

    public UserDto deleteUser(int id) throws UserDoesNotExistException {
        Optional<User> userToDelete = repository.findById(id);

        if (userToDelete.isPresent()) {
            repository.delete(userToDelete.get());
            return mapper.toDto(userToDelete.get());
        }
        throw new UserDoesNotExistException("User with id: " + id + " does not exist");
    }
}
