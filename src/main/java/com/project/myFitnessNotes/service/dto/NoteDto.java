package com.project.myFitnessNotes.service.dto;

import com.project.myFitnessNotes.model.Product;
import com.project.myFitnessNotes.model.User;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class NoteDto {
    private Integer id;
    private String text;
    private OffsetDateTime createdAt;
    private OffsetDateTime updateAt;

    private Integer userId;
    private List<Integer> productIds;

}
