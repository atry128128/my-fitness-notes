package com.project.myFitnessNotes.service.dto;

import com.project.myFitnessNotes.model.enums.ProductType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ProductDto {
    private Integer code;
    private String name;
    private ProductType type;
    private Integer amountOfCalories;
    private Double amountOfProtein;
    private Double amountOfCarbohydrates;
    private Double amountOfFat;
    private String productProducer;

    private List<Integer> userIds;
    private List<Integer> noteIds;


}
