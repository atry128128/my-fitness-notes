package com.project.myFitnessNotes.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UpdateUserDto {
    private String password;
    private String name;
    private String surname;
    private Integer weight;
    private Integer height;

    private List<Integer> notesIds;
    private List<Integer> productsIds;
}
