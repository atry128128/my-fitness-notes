package com.project.myFitnessNotes.service.dto;

import com.project.myFitnessNotes.model.Product;
import com.project.myFitnessNotes.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateUpdateNoteDto {
    private String text;

    private int userId;
    private List<Integer> productsIds;




}
