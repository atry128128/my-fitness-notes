package com.project.myFitnessNotes.service.dto;

import com.project.myFitnessNotes.model.enums.Sex;
import com.project.myFitnessNotes.model.enums.UserRole;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDto {
    private Integer id;
    private String login;
    private String name;
    private String surname;
    private UserRole role;
    private Sex sex;
    private Integer weight;
    private Integer height;
    private LocalDate dateOfBirth;
    private LocalDate dateOfFirstEntry;

    private List<Integer> notesIds;
    private List<Integer> productsIds;
}
