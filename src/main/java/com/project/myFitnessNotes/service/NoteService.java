package com.project.myFitnessNotes.service;

import com.project.myFitnessNotes.model.Note;
import com.project.myFitnessNotes.model.User;
import com.project.myFitnessNotes.repository.NoteRepository;
import com.project.myFitnessNotes.repository.UserRepository;
import com.project.myFitnessNotes.service.dto.CreateUpdateNoteDto;
import com.project.myFitnessNotes.service.dto.NoteDto;
import com.project.myFitnessNotes.service.exception.NoteDoesNotExistException;
import com.project.myFitnessNotes.service.mapper.NoteToMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class NoteService {

    private Integer id = 1;
    @Autowired
    private NoteToMapper mapper;
    @Autowired
    private NoteRepository repository;
    @Autowired
    private UserRepository repositoryUser;


    public List<NoteDto> getAllNote() {
        List<Note> notes = repository.findAll();
        List<NoteDto> notesDto = new ArrayList<>();
        for (Note note : notes) {
            NoteDto noteDto = mapper.toDto(note);
            notesDto.add(noteDto);
        }
        return notesDto;
    }

    public NoteDto findById(int id) throws NoteDoesNotExistException {
        Optional<Note> optionalNote = repository.findById(id);

        if (optionalNote.isPresent()) {
            return mapper.toDto(optionalNote.get());
        }
        throw new NoteDoesNotExistException("Note with id: " + id + "is not exist.");
    }

    public NoteDto addNewNote(CreateUpdateNoteDto createNote) {
        repository.save(mapper.toModel(createNote));

        User user = repositoryUser.findById(createNote.getUserId()).get();
        Note note = new Note(id++, createNote.getText(), OffsetDateTime.now(), null,null,user);
        return mapper.toDto(note);
    }

    public NoteDto changeNote(int id, CreateUpdateNoteDto updateNote) throws NoteDoesNotExistException {
        List<Note> notesFromDb = repository.findAll();
        for (Note note : notesFromDb) {
            if (id == note.getId()) {
                note.setText(updateNote.getText());
                note.setUpdateAt(OffsetDateTime.now());
            }
            Note changedNote = repository.save(note);
            return mapper.toDto(changedNote);
        }
        throw new NoteDoesNotExistException("Note with id: " + id + " does not exist");
    }

    public NoteDto deleteNote(int id) throws NoteDoesNotExistException {
        List<Note> noteToDelete = repository.findAll();
        for (Note note : noteToDelete) {
            if (id == note.getId()) {
                noteToDelete.remove(note);

                return mapper.toDto(note);
            }
        }
        throw new NoteDoesNotExistException("Note with id: " + id + " does not exist");
    }
}
