package com.project.myFitnessNotes.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class BusyNameProductException extends Exception {
    public BusyNameProductException(String message) {
        super(message);
    }
}
