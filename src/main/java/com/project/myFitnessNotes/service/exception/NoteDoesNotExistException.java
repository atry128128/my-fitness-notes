package com.project.myFitnessNotes.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class NoteDoesNotExistException extends Exception {
    public NoteDoesNotExistException(String message) {
        super(message);
    }
}
