package com.project.myFitnessNotes.service.mapper;

import com.project.myFitnessNotes.model.Note;
import com.project.myFitnessNotes.model.Product;
import com.project.myFitnessNotes.model.User;
import com.project.myFitnessNotes.service.dto.CreateUserDto;
import com.project.myFitnessNotes.service.dto.UserDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class UserToMapper {
    private static int idCounter = 0;


    public UserDto toDto(User user) {

        List<Integer> productsList = new ArrayList<>();
        if(user.getProducts() !=null){
            for(Product product: user.getProducts()){
                productsList.add(product.getCode());
            }
        }
        List<Integer> notesList = new ArrayList<>();
        if(user.getNotes() !=null){
            for(Note note: user.getNotes()){
                productsList.add(note.getId());
            }
        }

        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .name(user.getName())
                .surname(user.getSurname())
                .role(user.getRole())
                .sex(user.getSex())
                .weight(user.getWeight())
                .height(user.getHeight())
                .dateOfBirth(user.getDateOfBirth())
                .dateOfFirstEntry(user.getDateOfFirstEntry())
                .notesIds(notesList)
                .productsIds(productsList)
                .build();

    }


    public User toModel(CreateUserDto createUserDto) {

        return User.builder()
                .id(idCounter++)
                .login(createUserDto.getLogin())
                .name(createUserDto.getName())
                .surname(createUserDto.getSurname())
                .role(createUserDto.getRole())
                .sex(createUserDto.getSex())
                .weight(createUserDto.getWeight())
                .height(createUserDto.getHeight())
                .dateOfBirth(createUserDto.getDateOfBirth())
                .dateOfFirstEntry(createUserDto.getDateOfFirstEntry())
                .notes(null)
                .products(null)
                .build();

    }
}
