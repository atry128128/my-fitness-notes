package com.project.myFitnessNotes.service;

import com.project.myFitnessNotes.model.Product;
import com.project.myFitnessNotes.repository.ProductRepository;
import com.project.myFitnessNotes.service.dto.CreateUpdateProductDto;
import com.project.myFitnessNotes.service.dto.ProductDto;
import com.project.myFitnessNotes.service.exception.BusyNameProductException;
import com.project.myFitnessNotes.service.exception.ProductDoesNotExistException;
import com.project.myFitnessNotes.service.mapper.ProductToMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private Integer code = 0;
    @Autowired
    private ProductToMapper mapper;
    @Autowired
    private ProductRepository repository;


    public List<ProductDto> getAllProducts() {
        List<Product> productsFromDb = repository.findAll();
        List<ProductDto> productsDto = new ArrayList<>();
        for (Product product : productsFromDb) {
            ProductDto productDto = mapper.toDto(product);
            productsDto.add(productDto);
        }
        return productsDto;
    }

    public ProductDto findProductByCode(int code) throws ProductDoesNotExistException {
        Optional<Product> productFromDb = repository.findById(code);

        if (productFromDb.isPresent()) {
            return mapper.toDto(productFromDb.get());
        }

        throw new ProductDoesNotExistException("Product with code: " + code + " does not exist");
    }

    public ProductDto findByName(String name) throws ProductDoesNotExistException {
        Optional<Product> productFromDb = repository.findById(Integer.parseInt(name));

        if (productFromDb.isPresent()) {
            return mapper.toDto(productFromDb.get());
        }
        throw new ProductDoesNotExistException("Product with name: " + name + " does not exist");
    }

    public ProductDto addNewProduct(CreateUpdateProductDto createProduct) throws BusyNameProductException {
        List<Product> productsFromDb = repository.findAll();
        for (Product product : productsFromDb) {
            if (product.getName().equalsIgnoreCase(createProduct.getName())) {
                throw new BusyNameProductException("Name: " + createProduct.getName() + " is busy. Choose another.");
            }
        }
        repository.save(mapper.toModel(createProduct));

        Product product = new Product(code++, createProduct.getName(), createProduct.getType(), createProduct.getAmountOfCalories(),
                createProduct.getAmountOfProtein(), createProduct.getAmountOfCarbohydrates(), createProduct.getAmountOfFat(),
                createProduct.getProductProducer(),null,null);
        return mapper.toDto(product);

    }

    public ProductDto changeProduct(int code, CreateUpdateProductDto updateProduct) throws ProductDoesNotExistException {
        List<Product> productsFromDb = repository.findAll();
        for (Product product : productsFromDb) {
            if (code == product.getCode()) {
                product.setName(updateProduct.getName());
                product.setType(updateProduct.getType());
                product.setAmountOfCalories(updateProduct.getAmountOfCalories());
                product.setAmountOfProtein(updateProduct.getAmountOfProtein());
                product.setAmountOfCarbohydrates(updateProduct.getAmountOfCarbohydrates());
                product.setAmountOfFat(updateProduct.getAmountOfFat());
                product.setProductProducer(updateProduct.getProductProducer());
            }
            Product changedProduct = repository.save(product);
            return mapper.toDto(changedProduct);

        }
        throw new ProductDoesNotExistException("Product with code: " + code + " does not exist");
    }

    public ProductDto deleteProduct(int code) throws ProductDoesNotExistException {
        Optional<Product> productToDelete = repository.findById(code);
        if (productToDelete.isPresent()) {

            repository.delete(productToDelete.get());
            return mapper.toDto(productToDelete.get());
        }
        throw new ProductDoesNotExistException("Product with code: " + code + " does not exist");
    }
}
