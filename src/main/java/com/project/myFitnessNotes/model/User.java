package com.project.myFitnessNotes.model;

import com.project.myFitnessNotes.model.enums.Sex;
import com.project.myFitnessNotes.model.enums.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class User {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String login;
    private String password;
    private String name;
    private String surname;
    private UserRole role;
    private Sex sex;
    private Integer weight;
    private Integer height;
    private LocalDate dateOfBirth;
    private LocalDate dateOfFirstEntry;

    @ManyToMany
    private List<Product> products;
    @OneToMany(mappedBy = "user")
    private List<Note> notes;

}
