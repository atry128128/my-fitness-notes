package com.project.myFitnessNotes.model;

import com.project.myFitnessNotes.model.enums.ProductType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.ManyToMany;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Product {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer code;
    private String name;
    private ProductType type;
    private Integer amountOfCalories;
    private Double amountOfProtein;
    private Double amountOfCarbohydrates;
    private Double amountOfFat;
    private String productProducer;

    @ManyToMany(mappedBy = "products")
    private List<User> usersList;
    @ManyToMany(mappedBy = "products")
    private List<Note> notes;



}
