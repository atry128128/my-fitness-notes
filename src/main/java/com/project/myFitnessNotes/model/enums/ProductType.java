package com.project.myFitnessNotes.model.enums;

public enum ProductType {
    SNACK,
    FRUIT,
    VEGETABLES,
    MEET,
    PROTEIN_PRODUCT,
    DRINKS
}
