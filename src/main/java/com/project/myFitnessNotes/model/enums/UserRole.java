package com.project.myFitnessNotes.model.enums;

public enum UserRole {
    USER,
    MODERATOR,
    ADMIN
}
