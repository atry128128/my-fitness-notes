package com.project.myFitnessNotes.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
public class Note {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String text;
    private OffsetDateTime createdAt;
    private OffsetDateTime updateAt;

    @ManyToMany
    private List<Product> products;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;



}