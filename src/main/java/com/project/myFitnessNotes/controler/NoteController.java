package com.project.myFitnessNotes.controler;

import com.project.myFitnessNotes.service.NoteService;
import com.project.myFitnessNotes.service.dto.CreateUpdateNoteDto;
import com.project.myFitnessNotes.service.dto.NoteDto;
import com.project.myFitnessNotes.service.exception.NoteDoesNotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Component
@Controller
@RequestMapping("/api/note")
public class NoteController {
    @Autowired
    private NoteService noteService;

    @GetMapping
    public List<NoteDto> getAllNote() {
        return noteService.getAllNote();
    }

    @GetMapping("{id}")
    public NoteDto findNoteById(@PathVariable int id) throws NoteDoesNotExistException {
        return noteService.findById(id);
    }

    @PostMapping
    public NoteDto addNewNote(@RequestBody CreateUpdateNoteDto createNote) {
        return noteService.addNewNote(createNote);
    }

    @PutMapping("{id}")
    public NoteDto updateNote(@PathVariable int id, @RequestBody CreateUpdateNoteDto updateNote) throws NoteDoesNotExistException {
        return noteService.changeNote(id, updateNote);
    }

    @DeleteMapping("{id}")
    public NoteDto deleteNote(@PathVariable int id) throws NoteDoesNotExistException {
        return noteService.deleteNote(id);
    }
}
