package com.project.myFitnessNotes.controler;

import com.project.myFitnessNotes.service.UserService;
import com.project.myFitnessNotes.service.dto.CreateUserDto;
import com.project.myFitnessNotes.service.dto.UpdateUserDto;
import com.project.myFitnessNotes.service.dto.UserDto;
import com.project.myFitnessNotes.service.exception.BusyLoginException;
import com.project.myFitnessNotes.service.exception.UserDoesNotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("{id}")
    public UserDto findUserById(@PathVariable int id) throws UserDoesNotExistException {
        return userService.findUserById(id);
    }

    @PostMapping
    public UserDto addNewuser(@RequestBody CreateUserDto createUser) throws BusyLoginException {
        return userService.addNewUser(createUser);
    }

    @PutMapping("{id}")
    public UserDto updateuser(@PathVariable int id, @RequestBody UpdateUserDto updateUser) throws UserDoesNotExistException {
        return userService.changeUser(id, updateUser);
    }

    @DeleteMapping("{id}")
    public UserDto deleteUser(@PathVariable int id) throws UserDoesNotExistException {
        return userService.deleteUser(id);
    }

}
