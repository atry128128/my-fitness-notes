package com.project.myFitnessNotes.controler;

import com.project.myFitnessNotes.service.ProductService;
import com.project.myFitnessNotes.service.dto.CreateUpdateProductDto;
import com.project.myFitnessNotes.service.dto.ProductDto;
import com.project.myFitnessNotes.service.exception.BusyNameProductException;
import com.project.myFitnessNotes.service.exception.ProductDoesNotExistException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping
    public List<ProductDto> getAllProduct() {
        return productService.getAllProducts();
    }

    @GetMapping("{code}")
    public ProductDto findProductByCode(@PathVariable int code) throws ProductDoesNotExistException {
        return productService.findProductByCode(code);
    }

    @GetMapping("/name")
    public ProductDto findProductByName(@RequestParam String name) throws ProductDoesNotExistException {
        return productService.findByName(name);
    }

    @PostMapping
    public ProductDto addNewProduct(@RequestBody CreateUpdateProductDto createProduct) throws BusyNameProductException {
        return productService.addNewProduct(createProduct);
    }

    @PutMapping("{code}")
    public ProductDto updateProduct(@PathVariable int code, @RequestBody CreateUpdateProductDto updateProduct) throws ProductDoesNotExistException {
        return productService.changeProduct(code, updateProduct);
    }

    @DeleteMapping("{code}")
    public ProductDto deleteProduct(@PathVariable int code) throws ProductDoesNotExistException {
        return productService.deleteProduct(code);
    }
}
